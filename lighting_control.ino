//*** Don't forget to edit config.h file!!!

#include <ESP8266WiFi.h>
#include <DHT.h>
#include "config.h"
#include <ArduinoJson.h>

//DHT Definitions
#define DHTTYPE DHT22
#define DHTPIN D3
#define LED D2
void dht_wrapper();
DHT sensor(DHTPIN, DHTTYPE);

WiFiServer server(80);

int pwmValue = 255;
boolean status = false;

struct dht_data {
  float temperature;
  float humidity;
};

dht_data * data;

void handleWiFi(WiFiClient client) {
  Serial.println("New client conntected");

  while (!client.available()) {
    delay(1);
  }

  String req = client.readStringUntil('\r');
  Serial.println(req);

  client.flush();

  //JSON
  if (req.indexOf("/get.json") != -1) {

    DynamicJsonBuffer jb(JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2));

    JsonObject& root = jb.createObject();
    updateDHT();
    root["temp"].set(data->temperature);
    root["hum"].set(data->humidity);

    JsonArray& arr = root.createNestedArray("lights");

    JsonObject& light1 = arr.createNestedObject();
    light1["status"] = status;
    light1["value"] = pwmValue;

    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: application/json");
    client.println("");
    root.printTo(client);


  } else {
    //*** OLD Method compatibility
    String answer = matchRequest(req);
    //String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nOK!";
    String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
    s += answer;
    client.print(s);
  }


  delay(1);

  Serial.println("Client disconnected");

}


int convertValueToInt(String fragment) {

  int i = 0;
  while (fragment[i] != '=') {
    i++;
  }

  String value = "";
  i++;

  while (fragment[i] != '&' && i < fragment.length()) {
    value += fragment[i];
    i++;
  }

  Serial.println("Converted value");
  Serial.println(value);
  return value.toInt();

}


String matchRequest(String req) {


  // SAMPLE /get
  // SAMPLE /set?status=1&pwm=251

  String answer = "";
  //1. GET VALUES

  if (req.indexOf("/get") != -1) {
    answer += "status=";
    answer += status;
    answer += ";pwm=";
    answer += pwmValue;
    answer += ";temp=";
    answer += sensor.readTemperature();
    answer += ";hum=";
    answer += sensor.readHumidity();
    answer += ";";

    return answer;

  } else if (req.indexOf("/set") != -1) {

    //If string contains data about PWM
    if (req.indexOf("pwm") != -1) {
      updatePWMValue(convertValueToInt(req.substring(req.indexOf("pwm"))));
    }
    //If string contains data about status
    if (req.indexOf("status") != -1) {
      updateStatus(convertValueToInt(req.substring(req.indexOf("status"))));
    }

    return "OK";
  }

  return "FAIL";

}

void updatePWMValue(int pwm) {

  if (pwm >= 0 && pwm <= 1023) {
    pwmValue = pwm;
    analogWrite(LED, pwmValue);
    Serial.print("\nSet PWM to: ");
    Serial.print(pwmValue);
    Serial.println();
  }
}

void updateStatus(boolean state) {
  status = state;

  if (status) {
    analogWrite(LED, pwmValue);
  } else {
    analogWrite(LED, 0);
  }

}

void updateDHT() {
  float temp_current = sensor.readTemperature();
  if (temp_current) {
    data->temperature = temp_current;
    data->humidity = sensor.readHumidity();
  }
}


void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(LED, OUTPUT);
  //digitalWrite(LED,HIGH);
  analogWrite(LED, 1023);

  data = new dht_data;
  data->temperature = -99;
  data->humidity = 0;

  Serial.println("Connecting to Wifi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);


  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println(".");
  }

  Serial.println("WL_Connected");

  server.begin();

  Serial.println(WiFi.localIP());

}

void loop() {
  WiFiClient client = server.available();

  if (!client) {
    return;
  }
  else {
    handleWiFi(client);
  }

}
